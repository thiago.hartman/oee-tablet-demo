import React, { useEffect, useState } from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);

const labels = ["OK", "NG"];
const options = {
  label: "# of Votes",
  backgroundColor: ["rgba(75, 192, 192, 0.2)", "rgba(255, 99, 132, 0.2)"],
  borderColor: ["rgba(75, 192, 192, 1)", "rgba(255, 99, 132, 1)"],
  borderWidth: 1,
};

export function PieChart({ ok, ng }) {
  const [data, setData] = useState({
    labels: [...labels],
    datasets: [{ ...options }],
  });

  useEffect(() => {
    setData({
      labels: [...labels],
      datasets: [
        {
          ...options,
          data: [ok, ng],
        },
      ],
    });
  }, [ng, ok]);
  return <Pie data={data} />;
}
