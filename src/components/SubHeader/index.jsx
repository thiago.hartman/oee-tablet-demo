import { Container, Grid, Typography } from "@material-ui/core";

export function SubHeader() {
  return (
    <Container
      maxWidth={false}
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "#F1F6F9",
        minHeight: "5vh",
      }}
    >
      <Grid container>
        <Grid
          item
          xs={4}
          style={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "center",
          }}
        >
          <span style={{ display: "flex" }}>
            <Typography style={{ fontWeight: 700 }}>Processo: </Typography>
            <Typography>Estator</Typography>
          </span>
        </Grid>
        <Grid
          item
          xs={4}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <span style={{ display: "flex" }}>
            <Typography style={{ fontWeight: 700 }}>Modelo: </Typography>
            <Typography>KXPOr</Typography>
          </span>
        </Grid>
        <Grid
          item
          xs={4}
          style={{
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
        >
          <span style={{ display: "flex" }}>
            <Typography style={{ fontWeight: 700 }}>Turno: </Typography>
            <Typography>1º - 03/10/2022</Typography>
          </span>
        </Grid>
      </Grid>
    </Container>
  );
}
