import { Grid, Typography } from "@material-ui/core";
import logo from "../../assets/logo.png";

export function Appbar() {
  return (
    <Grid
      item
      sx
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "60px",
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
      }}
    >
      <img
        src={logo}
        alt="Core"
        style={{ position: "absolute", left: "57px", width: "48px" }}
      />
      <Typography variant="h6">CONFIGURAÇÃO DE SETUP</Typography>
    </Grid>
  );
}
