import "./App.css";
import { Container } from "@material-ui/core";
import { Appointments } from "./pages/Appointments";
import { Appbar } from "./components/Appbar";
import { SubHeader } from "./components/SubHeader";

function App() {
  return (
    <Container
      maxWidth={false}
      disableGutters
      style={{
        background: "#fff",
        minHeight: "100vh",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",

        flexDirection: "column",
      }}
    >
      <Appbar />
      <SubHeader />
      <Appointments />
    </Container>
  );
}

export default App;
