import { Button, Container, Grid, Typography } from "@material-ui/core";
import { useState } from "react";
import { PieChart } from "../../components/Charts/Pie";

export function Appointments() {
  const [countOK, setCountOK] = useState(0);
  const [countNG, setCountNG] = useState(0);
  let coOK = 0
  let coNG = 0

  // function handleButton(type) {
  //   console.log('entrei aqui')
  //   if (type === "NG") {
  //     setCountNG(coNG + 1);
  //   } else if (type === "OK") {
  //     setCountOK(coOK + 1);
  //   }
  // }
  function testeUsb() {
    //funcao que solicita autorizacao do usuario para acessar serial
    if (navigator.serial) {
      connectSerial();
    } else {
      alert("not supported");
    }
  }

  async function connectSerial() {
    try {
      const port = await navigator.serial.requestPort();
      await port.open({ baudRate: 9600 });

      const decoder = new TextDecoderStream();

      port.readable.pipeTo(decoder.writable);

      const inputStream = decoder.readable;
      const reader = inputStream.getReader();
      while (true) {
        const { value, done } = await reader.read();
        if (value) {
          if (value == 1) {
            coOK = coOK + 1
            setCountOK(coOK);
          } else if (value == 2) {
            coNG = coNG + 1
            setCountNG(coNG);
          }
        }
        if (done) {
          reader.releaseLock();
          break;
        }
      }
    } catch (error) {
      alert(error);
    }
  }

  return (
    <>
      <Container
        maxWidth={false}
        style={{ background: "#E5E5E5", flexGrow: 1 }}
        disableGutters
      >
        <Grid container justifyContent="space-evenly" spacing={0}>
          <Grid container item xs={12} justifyContent="center">
            <Button variant="contained" onClick={testeUsb}>
              IDENTIFICAR PORTAS
            </Button>
          </Grid>
          <Grid>
            <Button
              variant="contained"
              style={{
                height: "128px",
                width: "266.78px",
                background: "#5CB85C",
                mixBlendMode: "darken",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                bordeRadius: "3px",
              }}
              onClick={() => {
                handleButton("OK");
              }}
            >
              <Typography
                variant="h1"
                style={{ color: "#FFF", fontSize: "40px", fontWeight: "700" }}
              >
                OK {countOK}
              </Typography>
            </Button>
          </Grid>
          <Grid>
            <Button
              variant="contained"
              style={{
                background: "#DC0032",
                height: "128px",
                width: "266.78px",
                mixBlendMode: "darken",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                bordeRadius: "3px",
              }}
              onClick={() => {
                handleButton("NG");
              }}
            >
              <Typography
                variant="h1"
                style={{ color: "#FFF", fontSize: "40px", fontWeight: "700" }}
              >
                {" "}
                NG {countNG}
              </Typography>
            </Button>
          </Grid>
          {countOK || countNG ? (
            <Grid container item justifyContent="center" xs={12}>
              <div style={{ width: "400px" }}>
                <PieChart ok={countOK} ng={countNG} />
              </div>
            </Grid>
          ) : (
            <></>
          )}
        </Grid>
      </Container>
    </>
  );
}
